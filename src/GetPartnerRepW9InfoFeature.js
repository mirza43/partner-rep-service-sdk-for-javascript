import {inject} from 'aurelia-dependency-injection';
import PartnerRepServiceSdkConfig from './partnerRepServiceSdkConfig';
import {HttpClient} from 'aurelia-http-client';
import W9InfoView from './w9InfoView';
import W9InfoViewFactory from './w9InfoViewFactory';

@inject(PartnerRepServiceSdkConfig, HttpClient)
class GetPartnerRepW9InfoFeature {

    _config:PartnerRepServiceSdkConfig;

    _httpClient:HttpClient;

    constructor(config,
                httpClient) {

        if (!config) {
            throw 'config required';
        }
        this._config = config;

        if (!httpClient) {
            throw 'httpClient required';
        }
        this._httpClient = httpClient;
    }

    /**
     * gets the w9 info with the provided id.
     * @param {string} partnerRepId
     * @param {string} accessToken
     * @returns {Promise.<W9InfoView>}
     */
    execute(partnerRepId:string,
            accessToken:string):Promise<W9InfoView> {

        return this._httpClient
            .createRequest(`/partner-reps/w9info/`+`${partnerRepId}`)
            .asGet()
            .withBaseUrl(this._config.precorConnectApiBaseUrl)
            .withHeader('Authorization', `Bearer ${accessToken}`)
            .withHeader('Content-Type', `application/json`)
            .send()
            .then(response => {
                    if(response.response){
                        return new W9InfoView(
                            response.content.userId,
                            response.content.isExists
                        )
                    }else {
                        return null;
                    }
                }
            );
    }
}

export default GetPartnerRepW9InfoFeature;