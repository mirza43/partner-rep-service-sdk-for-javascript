
/**
 * The most detailed view of a bank info
 * @class {BankInfoView}
 */
export default class BankInfoView {

    _id:string;

    _isExists:boolean;


    /**
     *
     * @param {string} id
     * @param {boolean} isExists
     */
    constructor(id:string,
                isExists:boolean
    ) {

        this._id = id;

        this._isExists = isExists;

    }

    /**
     * @returns {string}
     */
    get id():string {
        return this._id;
    }

    /**
     * @returns {boolean}
     */
    get isExists():boolean {
        return this._isExists;
    }


    toJSON() {
        return {
            id: this._id,
            isExists: this._isExists
        }
    }
};