import {inject} from 'aurelia-dependency-injection';
import PartnerRepServiceSdkConfig from './partnerRepServiceSdkConfig';
import {HttpClient} from 'aurelia-http-client';
import PartnerRepView from './partnerRepView';
import PartnerRepViewFactory from './partnerRepViewFactory';

@inject(PartnerRepServiceSdkConfig, HttpClient)
class GetPartnerRepWithIdFeature {

    _config:PartnerRepServiceSdkConfig;

    _httpClient:HttpClient;

    constructor(config,
                httpClient) {

        if (!config) {
            throw 'config required';
        }
        this._config = config;

        if (!httpClient) {
            throw 'httpClient required';
        }
        this._httpClient = httpClient;
    }

    /**
     * Gets the partner rep with the provided id
     * @param {string} id
     * @param {string} accessToken
     * @returns {Promise.<PartnerRepView>}
     */
    execute(id:string,
            accessToken:string):Promise<PartnerRepView> {

        return this._httpClient
            .createRequest(`partner-reps/${id}`)
            .asGet()
            .withBaseUrl(this._config.precorConnectApiBaseUrl)
            .withHeader('Authorization', `Bearer ${accessToken}`)
            .send()
            .then(response => PartnerRepViewFactory.construct(response.content));
    }
}

export default GetPartnerRepWithIdFeature;