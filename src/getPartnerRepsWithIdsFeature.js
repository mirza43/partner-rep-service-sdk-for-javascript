import {inject} from 'aurelia-dependency-injection';
import PartnerRepServiceSdkConfig from './partnerRepServiceSdkConfig';
import {HttpClient} from 'aurelia-http-client';
import PartnerRepSynopsisView from './partnerRepSynopsisView';

@inject(PartnerRepServiceSdkConfig, HttpClient)
class GetPartnerRepsWithIdsFeature {

    _config:PartnerRepServiceSdkConfig;

    _httpClient:HttpClient;

    constructor(config,
                httpClient) {

        if (!config) {
            throw 'config required';
        }
        this._config = config;

        if (!httpClient) {
            throw 'httpClient required';
        }
        this._httpClient = httpClient;

    }

    /**
     * gets the partner reps with the provided ids.
     * @param {string[]} partnerRepIds
     * @param {string} accessToken
     * @returns {Promise.<PartnerRepSynopsisView[]>}
     */
    execute(partnerRepIds:string[],
            accessToken:string):Promise<PartnerRepSynopsisView[]> {

        return this._httpClient
            .createRequest(`/partner-reps/list`)
            .asPost()
            .withBaseUrl(this._config.precorConnectApiBaseUrl)
            .withHeader('Authorization', `Bearer ${accessToken}`)
            .withHeader('Content-Type', `application/json`)
            .withContent(JSON.stringify(partnerRepIds))
            .send()
            .then(response =>
                Array.from(
                    response.content,
                    contentItem =>
                        new PartnerRepSynopsisView(
                            contentItem.firstName,
                            contentItem.lastName,
                            contentItem.id,
                            contentItem.sapAccountNumber,
                            contentItem.groupId
                        )
                )
            );
    }
}

export default GetPartnerRepsWithIdsFeature;