export default class PartnerRepBankInfoUpdate {

    _transactionId:string;

    constructor(transactionId:string) {

        if (!transactionId) {
            throw new TypeError('transactionId required');
        }
        this._transactionId = transactionId;

    }

    get transactionId():string {
        return this._transactionId;
    }

}