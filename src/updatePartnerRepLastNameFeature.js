import {inject} from 'aurelia-dependency-injection';
import PartnerRepServiceSdkConfig from './partnerRepServiceSdkConfig';
import {HttpClient} from 'aurelia-http-client';
import UpdatePartnerRepLastNameReq from './updatePartnerRepLastNameReq';

@inject(PartnerRepServiceSdkConfig, HttpClient)
class UpdatePartnerRepLastNameFeature {

    _config:PartnerRepServiceSdkConfig;

    _httpClient:HttpClient;

    constructor(config,
                httpClient) {

        if (!config) {
            throw 'config required';
        }
        this._config = config;

        if (!httpClient) {
            throw 'httpClient required';
        }
        this._httpClient = httpClient;

    }

    /**
     * Gets a transient url where a partner reps bank info can be updated
     * @param {UpdatePartnerRepLastNameReq} request
     * @param {string} accessToken
     * @returns {Promise}
     */
    execute(request:UpdatePartnerRepLastNameReq,
            accessToken:string):Promise {

        return this._httpClient
            .createRequest(`partner-reps/${request.id}/last-name`)
            .asPut()
            .withBaseUrl(this._config.precorConnectApiBaseUrl)
            .withHeader('Authorization', `Bearer ${accessToken}`)
            .withContent(request.lastName)
            .send();
    }
}

export default UpdatePartnerRepLastNameFeature;