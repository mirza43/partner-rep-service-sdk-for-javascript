import {PostalAddress} from 'postal-object-model';
import PartnerRepW9Update from '../src/partnerRepW9Update';
import PartnerRepBankInfoUpdate from '../src/partnerRepBankInfoUpdate';

/**
 * dummy objects (see: http://xunitpatterns.com/Dummy%20Object.html)
 */
export default {
    firstName: 'firstName',
    lastName: 'lastName',
    phoneNumber: '0000000000',
    accountId: '001A000000MGmkFIAT',
    sapVendorNumber: '0000000000',
    userId: '00u5h8rhbtzPEtCH20h7',
    emailAddress: 'email@test.com',
    url: 'https://dummy-url.com',
    postalAddress: new PostalAddress(
        'street',
        'city',
        'WA',
        'postalCode',
        'US'
    ),
    transactionId: 'transactionId',
    groupId: 'Dealer',
    sapAccountNumber : '156855'
};