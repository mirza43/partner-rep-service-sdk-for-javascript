import PartnerRepServiceSdk,
{
    AddPartnerRepReq,
    GetPartnerRepBankInfoUpdateUrlReq,
    GetPartnerRepW9UpdateUrlReq
} from '../../src/index';
import factory from './factory';
import PartnerRepView from '../../src/partnerRepView';
import PartnerRepSynopsisView from '../../src/partnerRepSynopsisView';
import config from './config';
import dummy from '../dummy';

const urlRegex = /[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&\/=]*)/;

/*
 tests
 */
describe('Index module', () => {


    describe('default export', () => {
        it('should be PartnerRepServiceSdk constructor', () => {
            /*
             act
             */
            const objectUnderTest =
                new PartnerRepServiceSdk(config.partnerRepServiceSdkConfig);

            /*
             assert
             */
            expect(objectUnderTest).toEqual(jasmine.any(PartnerRepServiceSdk));

        });
    });

    describe('instance of default export', () => {

        describe('addPartnerRep method', () => {
            it('should return partnerRepId', (done) => {
                /*
                 arrange
                 */
                const objectUnderTest =
                    new PartnerRepServiceSdk(config.partnerRepServiceSdkConfig);

                let returnedPartnerRepId;

                const accountId =
                    config.accountIdOfExistingAccountWithAnSapAccountNumber;

                /*
                 act
                 */
                const actPromise =
                    objectUnderTest
                        .addPartnerRep(
                            new AddPartnerRepReq(
                                dummy.firstName,
                                dummy.lastName,
                                factory.constructUniqueEmailAddress(),
                                accountId
                            ),
                            factory.constructValidPartnerRepOAuth2AccessToken(
                                accountId,
                                dummy.userId
                            )
                        )
                        .then(partnerRepId => {
                            returnedPartnerRepId = partnerRepId;
                        });

                /*
                 assert
                 */

                actPromise
                    .then(() => {
                        expect(returnedPartnerRepId).toBeTruthy();
                        done();
                    })
                    .catch(error=> done.fail(JSON.stringify(error)));

            },20000);
        });
        describe('getPartnerRepW9UpdateUrl method', () => {
            it('should return a valid url', (done) => {
                /*
                 arrange
                 */
                const objectUnderTest =
                    new PartnerRepServiceSdk(
                        config.partnerRepServiceSdkConfig
                    );

                const accountId =
                    config.accountIdOfExistingAccountWithAnSapAccountNumber;

                let idOfSeededPartnerRep;
                let returnedUrl;

                // seed partner rep
                const arrangePromise =
                    objectUnderTest
                        .addPartnerRep(
                            new AddPartnerRepReq(
                                dummy.firstName,
                                dummy.lastName,
                                factory.constructUniqueEmailAddress(),
                                accountId
                            ),
                            factory.constructValidPartnerRepOAuth2AccessToken(
                                accountId,
                                dummy.userId
                            )
                        )
                        .then(partnerRepId => {
                            idOfSeededPartnerRep = partnerRepId;
                        });

                /*
                 act
                 */
                const actPromise =
                    arrangePromise
                        .then(
                            () =>
                                objectUnderTest
                                    .getPartnerRepW9UpdateUrl(
                                        new GetPartnerRepW9UpdateUrlReq(
                                            idOfSeededPartnerRep,
                                            dummy.url
                                        ),
                                        factory.constructValidPartnerRepOAuth2AccessToken(
                                            accountId,
                                            idOfSeededPartnerRep
                                        )
                                    )
                        )
                        .then(partnerRepW9UpdateUrl => {
                            returnedUrl = partnerRepW9UpdateUrl;
                        });


                /*
                 assert
                 */
                actPromise
                    .then(() => {
                        expect(returnedUrl).toMatch(urlRegex);
                        done();
                    })
                    .catch(error=> done.fail(JSON.stringify(error)));

            }, 40000);
        });
        describe('getPartnerRepBankInfoUpdateUrl method', () => {
            it('should return a valid url', (done) => {
                /*
                 arrange
                 */
                const objectUnderTest =
                    new PartnerRepServiceSdk(
                        config.partnerRepServiceSdkConfig
                    );

                const accountId =
                    config.accountIdOfExistingAccountWithAnSapAccountNumber;

                let idOfSeededPartnerRep;
                let returnedUrl;

                // seed partner rep
                const arrangePromise =
                    objectUnderTest
                        .addPartnerRep(
                            new AddPartnerRepReq(
                                dummy.firstName,
                                dummy.lastName,
                                factory.constructUniqueEmailAddress(),
                                accountId
                            ),
                            factory.constructValidPartnerRepOAuth2AccessToken(
                                accountId,
                                dummy.userId
                            )
                        )
                        .then(partnerRepId => {
                            idOfSeededPartnerRep = partnerRepId;
                        });

                /*
                 act
                 */
                const actPromise =
                    arrangePromise
                        .then(
                            () =>
                                objectUnderTest
                                    .getPartnerRepBankInfoUpdateUrl(
                                        new GetPartnerRepBankInfoUpdateUrlReq(
                                            idOfSeededPartnerRep,
                                            dummy.url
                                        ),
                                        factory.constructValidPartnerRepOAuth2AccessToken(
                                            accountId,
                                            idOfSeededPartnerRep
                                        )
                                    )
                        )
                        .then(partnerRepBankInfoUpdateUrl => {
                            returnedUrl = partnerRepBankInfoUpdateUrl;
                        });


                /*
                 assert
                 */
                actPromise
                    .then(() => {
                        expect(returnedUrl).toMatch(urlRegex);
                        done();
                    })
                    .catch(error=> done.fail(JSON.stringify(error)));

            }, 40000);
        });
        describe('getPartnerRepWithId method', () => {
            it('should return expected PartnerRepView', () => {
                /*
                 arrange
                 */
                const objectUnderTest =
                    new PartnerRepServiceSdk(
                        config.partnerRepServiceSdkConfig
                    );

                const accountId =
                    config.accountIdOfExistingAccountWithAnSapAccountNumber;

                let expectedPartnerRepView;

                let actualPartnerRepView;

                // add a test partner rep
                const addPartnerRepReq =
                    new AddPartnerRepReq(
                        dummy.firstName,
                        dummy.lastName,
                        factory.constructUniqueEmailAddress(),
                        accountId
                    );

                const arrangePromise =
                    objectUnderTest
                        .addPartnerRep(
                            addPartnerRepReq,
                            factory.constructValidPartnerRepOAuth2AccessToken(
                                accountId,
                                dummy.userId
                            )
                        )
                        .then(partnerRepId => {

                            // construct expectedPartnerRepView
                            expectedPartnerRepView =
                                new PartnerRepView(
                                    partnerRepId,
                                    addPartnerRepReq.firstName,
                                    addPartnerRepReq.lastName,
                                    addPartnerRepReq.emailAddress,
                                    addPartnerRepReq.accountId,
                                    addPartnerRepReq.groupId,
                                    addPartnerRepReq.sapAccountNumber
                                );
                        });

                /*
                 act
                 */
                const actPromise =
                    arrangePromise
                        .then(
                            () =>
                                objectUnderTest
                                    .getPartnerRepWithId(
                                        expectedPartnerRepView.id,
                                        factory.constructValidPartnerRepOAuth2AccessToken(
                                            addPartnerRepReq.accountId,
                                            expectedPartnerRepView.id
                                        )
                                    )
                        )
                        .then(partnerRepView => {
                            actualPartnerRepView = partnerRepView;
                        });

                /*
                 assert
                 */
                actPromise
                    .then(() => {
                        expect(actualPartnerRepView).toBeTruthy();
                        //done();
                    })
                    .catch(error=> done.fail(JSON.stringify(error)));

            },40000);
        });
        describe('getPartnerRepsWithIds method', () => {
            it('returns expected number of results', () => {
                /*
                 arrange
                 */
                const objectUnderTest =
                    new PartnerRepServiceSdk(
                        config.partnerRepServiceSdkConfig
                    );

                const accountId =
                    config.accountIdOfExistingAccountWithAnSapAccountNumber;

                const expectedPartnerRepSynopsisViews = [];

                let actualPartnerRepSynopsisViews;

                // add test partner reps
                const addPartnerRepReq1 =
                    new AddPartnerRepReq(
                        dummy.firstName,
                        dummy.lastName,
                        factory.constructUniqueEmailAddress(),
                        accountId
                    );

                const addPartnerRepReq2 =
                    new AddPartnerRepReq(
                        dummy.firstName,
                        dummy.lastName,
                        factory.constructUniqueEmailAddress(),
                        accountId
                    );

                const arrangePromise =
                    Promise
                        .all(
                            [
                                objectUnderTest
                                    .addPartnerRep(
                                        addPartnerRepReq1,
                                        factory.constructValidPartnerRepOAuth2AccessToken(
                                            accountId,
                                            dummy.userId
                                        )
                                    ),
                                objectUnderTest
                                    .addPartnerRep(
                                        addPartnerRepReq2,
                                        factory.constructValidPartnerRepOAuth2AccessToken(
                                            accountId,
                                            dummy.userId
                                        )
                                    )
                            ]
                        )
                        .then(partnerRepIds => {
                                // construct expectedPartnerRepViews
                                expectedPartnerRepSynopsisViews.push(
                                    new PartnerRepSynopsisView(
                                        addPartnerRepReq1.firstName,
                                        addPartnerRepReq1.lastName,
                                        partnerRepIds[0]
                                    )
                                );
                                expectedPartnerRepSynopsisViews.push(
                                    new PartnerRepSynopsisView(
                                        addPartnerRepReq2.firstName,
                                        addPartnerRepReq2.lastName,
                                        partnerRepIds[1]
                                    )
                                );
                            }
                        );

                /*
                 act
                 */
                const actPromise =
                    arrangePromise
                        .then(
                            () =>
                                objectUnderTest
                                    .getPartnerRepsWithIds(
                                        expectedPartnerRepSynopsisViews
                                            .map(
                                                partnerRepSynopsisView =>
                                                    partnerRepSynopsisView.id
                                            ),
                                        factory.constructValidPartnerRepOAuth2AccessToken(
                                            accountId
                                        )
                                    )
                        )
                        .then(partnerRepSynopsisViews => {
                            actualPartnerRepSynopsisViews = partnerRepSynopsisViews;
                        });

                /*
                 assert
                 */
                actPromise
                    .then(() => {
                        expect(actualPartnerRepSynopsisViews.length)
                            .toEqual(expectedPartnerRepSynopsisViews.length);

                        //done();
                    })
                    .catch(error=> done.fail(JSON.stringify(error)));

            },40000);
        });
        describe('searchForPartnerRepWithEmailAddress method', () => {
            it('returns null for unused email address', done => {
                /*
                 arrange
                 */
                const unknownEmailAddress =
                    factory.constructUniqueEmailAddress();

                const objectUnderTest =
                    new PartnerRepServiceSdk(
                        config.partnerRepServiceSdkConfig
                    );

                /*
                 act
                 */
                const searchResultPromise =
                    objectUnderTest
                        .searchForPartnerRepWithEmailAddress(
                            unknownEmailAddress,
                            factory.constructValidPartnerRepOAuth2AccessToken()
                        );

                /*
                 assert
                 */

                searchResultPromise
                    .then(searchResult => {
                        expect(searchResult).toBeFalsy();
                        done();
                    })
                    .catch(error=> done.fail(JSON.stringify(error)));

            },20000);
        });

        describe('isBankInfoExistsForPartnerRep method', () => {
            it('returns BankInfoView', (done) => {
                /*
                 arrange
                 */

                const objectUnderTest =
                    new PartnerRepServiceSdk(
                        config.partnerRepServiceSdkConfig
                    );

                /*
                 act
                 */
                const actPromise =
                    objectUnderTest
                        .isBankInfoExistsForPartnerRep(
                            dummy.userId,
                            factory.constructValidPartnerRepOAuth2AccessToken()
                        );

                /*
                 assert
                 */

                actPromise
                    .then(BankInfoView => {
                        if(BankInfoView) {
                            expect(BankInfoView).toBeTruthy();
                        }else{
                            expect(BankInfoView).toBeFalsy()
                        }
                        done();
                    })
                    .catch(error=> done.fail(JSON.stringify(error)));

            },20000);
        });

        describe('isW9InfoExistsForPartnerRep method', () => {
            it('returns W9InfoView', (done) => {
                /*
                 arrange
                 */

                const objectUnderTest =
                    new PartnerRepServiceSdk(
                        config.partnerRepServiceSdkConfig
                    );

                /*
                 act
                 */
                const actPromise =
                    objectUnderTest
                        .isW9InfoExistsForPartnerRep(
                            dummy.userId,
                            factory.constructValidPartnerRepOAuth2AccessToken()
                        );

                /*
                 assert
                 */

                actPromise
                    .then(W9InfoView => {
                        if(W9InfoView) {
                            expect(W9InfoView).toBeTruthy();
                        }else{
                            expect(W9InfoView).toBeNull()
                        }
                        done();
                    })
                    .catch(error=> done.fail(JSON.stringify(error)));

            },20000);
        });
    });
});