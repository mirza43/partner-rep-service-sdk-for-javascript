import PartnerRepBankInfoUpdate from '../../src/partnerRepBankInfoUpdate';
import dummy from '../dummy';

describe('PartnerRepBankInfoUpdate class', () => {
    describe('constructor', () => {
        it('throws if transactionId is null', () => {
            /*
             arrange
             */
            const constructor = () => new PartnerRepBankInfoUpdate(null);

            /*
             act/assert
             */
            expect(constructor).toThrowError(TypeError, 'transactionId required');

        });
        it('sets transactionId', () => {
            /*
             arrange
             */
            const expectedTransactionId = dummy.transactionId;

            /*
             act
             */
            const objectUnderTest =
                new PartnerRepBankInfoUpdate(expectedTransactionId);

            /*
             assert
             */
            const actualTransactionId = objectUnderTest.transactionId;
            expect(actualTransactionId).toEqual(actualTransactionId);
        });
    });
});